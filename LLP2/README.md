# Low-Lavel-Programming

Task 2
--------------------------------------------
Вариант №3
- GraphQL
## Задание:
Использовать средство синтаксического анализа по выбору, реализовать модуль для разбора некоторого достаточного подмножества языка запросов GraphQL. Должна быть обеспечена возможность описания команд создания, выборки, модификации и удаления элементов данных.

## Установка и запуск

### Необходимые реквизиты

- gcc
- flex
- bison

### Запуск

```shell
$ sh run.sh
```

Запрос должен находиться в файле "query.gql"

## Структура проекта

Лексический анализ (`lex.l`) происходит с помощью `flex`. Символы превращаются в токены. 
Некоторые токены преобразуются в `enum`'ы или другие вспомогательные структуры:  

```c
enum Crud_operation {
    CRUD_QUERY = 0,
    CRUD_REMOVE,
    CRUD_INSERT,
    CRUD_UPDATE
};

enum Condition_code {
    OP_EQUAL = 0,
    OP_GREATER,
    OP_LESS,
    OP_NOT_GREATER,
    OP_NOT_LESS
};

enum Logic_op {
    OP_NOT = 0,
    OP_AND,
    OP_OR

};

enum Type {
    STRING_TYPE = 0,
    INTEGER_TYPE,
    BOOLEAN_TYPE
};

struct Header;
struct View;
struct Filter;
```

Синтаксический анализатор (`parse.y`), написанный на `bison`, принимает токены 
и строит синтаксическое дерево с помощью вспомогательных структур данных, 
описанных в `type.h`.

## Примеры запросов

Вставка
```
insert{
  Film(){
    year: 1,
    director: "Kris",
    Actors(id: {eq: "abc"}){
    }
  }
}
```
![insert](https://gitlab.se.ifmo.ru/kurnosovaiv/low_lavel_programming/-/raw/master/img/perser_insert.png)

Удаление
```
delete{
  Film(and: [year: {eq: 1}, not: [name: {eq: "Kris"}]]){
    year
  }
}
```
![insert](https://gitlab.se.ifmo.ru/kurnosovaiv/low_lavel_programming/-/raw/master/img/parser_delete.png)

Выборка
```
query{
  Film(not: [id: {ge: 5}]){
    year,
    director,
    Actors(and:[age:{lt:50},age:{gt:20}]){
    	name,
    	age,
    	status
    },
    Oskars(count:{ge:2}){
    	nomination,
    	year,
    	count
    }
  }
}
```
![insert](https://gitlab.se.ifmo.ru/kurnosovaiv/low_lavel_programming/-/raw/master/img/parser_query.png)

Изменение
```
update{
  Film(and: [year: {eq: 1}, not: [name: {eq: "Kris"}]]){
    year: 125
  }
}
```
![insert](https://gitlab.se.ifmo.ru/kurnosovaiv/low_lavel_programming/-/raw/master/img/parser_update.png)
