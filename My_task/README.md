# Запросы

1. MATCH (p:Me)-[r:RT_MENTIONS]->(n) return n.name, n.followers, p.name
2. MATCH (p:Tweet)-[r:TAGS]->(n) return p.id, n.name
3. MATCH (p:Me)-[r:RT_MENTIONS]->(n) where n.followers>10 and n.name starts with 'S' return n.name, n.followers, p.name
4. MATCH (p:Tweet)-[r:TAGS]->(n) where p.id > 1370000000000000000 and n.name starts with 'neo' return p.id, n.name
