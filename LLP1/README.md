# Low-Lavel-Programming

Task 1
--------------------------------------------
Вариант №3
•	Граф узлов с атрибутами
## Задание:
Создать модуль, реализующий хранение в одном файле данных (выборку, размещение и гранулярное обновление) информации общим объёмом от 10GB соответствующего варианту вида.

1.	Спроектировать структуры данных для представления информации в оперативной памяти
2.	Спроектировать представление данных с учётом схемы для файла данных и реализовать базовые операции для работы с ним
3.	Используя в сигнатурах только структуры данных из п.1, реализовать публичный интерфейс со следующими операциями над файлом данных…
4.	Реализовать тестовую программу для демонстрации работоспособности решения
5.	Результаты тестирования по п.4 представить в составе отчёта


## Описание модели на текущий момент (возможны изменения в процессе работы):

1. Схема
    - Состоит из заголовка и блоков таблиц
    - Заголовок содержит служебную информацию (количесство таблиц, адреса пустых блоков ...)
2. Таблица
    - Состоит из заголовка и записей
    - Занимает фиксированный блок памяти (имеет максимум по количеству записей), если памяти недостаточно (превышено максимальное количество записей) - выделяется новый блок (продолжение текущего), укзатель его помещается в предыдущую таблицу этого типа
    - Заголовок хранит служебную информацию (количество записей, ардрес на продолжение таблицы (если она имеется) ...)
3. Элемент таблицы (запись)
    - Служебная информация (id - ?)
    - Атрибуты
    - Связи
4. Атрибуы элемента
    - (Атрибуты будут объединены в список)
    - Название
    - Знчение

## Текущий прогресс:
- [x] Структуры представлений строк и значений
- [x] Структура атрибута (отдельный атрибут)
- [x] Структура представления всех атрибутов
- [x] Структура элемента таблицы (Node)
- [x] Структура таблицы (Graph)
- [x] Структура схемы (Schema)
- [x] **Работа с файлом бд**
- [x] Операции над атрибутами элементов таблицы
- [x] **Операции над элементами таблицы**
- [x] Операции над элементами схемы

