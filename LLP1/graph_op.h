#ifndef GRAPH_OP_H
#define GRAPH_OP_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "graph.h"

enum
{
    fSpace = 0,
    fString,
    fNode
} typesFileData;

enum
{
    aInt32 = 0,
    aFloat,
    aString,
    aBool
} typesAttr;

enum 
{
    opdNumber = 0,
    opdString,
    opdAttrName,
    opdCond
};

enum
{
    opEqual = 0,
    opNotEqual, 
    opLess, 
    opGreater, 
    opNot, 
    opAnd, 
    opOr
};
#endif //GRAPH_OP_H


