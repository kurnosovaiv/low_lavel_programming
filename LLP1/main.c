﻿#include "main.h"

void test_insert()
{
    printf("%s------------%s\n", WHITE, RESET);
    printf("TEST_INSERT %s|%s\n", WHITE, RESET);
    printf("%s------------%s\n", WHITE, RESET);

    double tdiff;
    char* Titles[nMovies] = { "The Shining", "7 moments of string", "The Green Mile", "The Shawshank Redemption", "A Nightmare on Elm Street", "Forrest Gump", "The House That Jack Built", "Interstellar", "Silent Hill", "The Illusionist" };
    char* Families[10] = { "Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio"};
    int i;

    initGraphsData("data.cfg");

    memDBScheme* Scheme = createScheme();

    nodeStruct* MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB* DB = createNew(Scheme, "graphs.mydb");
    clock_t tstart;
    clock_t tend;

    for (int j = 0; j <= 10; j++)
    {
        tstart = clock();

        for (i = 0; i < 1000; i++)
        {
            createNode(DB, MovieNodeType);
            setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Titles[i % 5]));
            setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
            postNode(DB, MovieNodeType);
        }
        tend = clock();
        tdiff = (double)(tend - tstart) / CLOCKS_PER_SEC;
        printf("Inserting %d rows took %f seconds\n", 1000 * j, tdiff);
    }

    rewindFirstNodes(DB, MovieNodeType);
    i = 0;
    while (openNode(DB, MovieNodeType))
    {
        int Year;
        char* Title;
        Year = getNodeAttr(DB, MovieNodeType, "Year");
        Title = getString(DB, getNodeAttr(DB, MovieNodeType, "Title"));
        register_free(1 + strlen(Title));
        free(Title);
        nextNode(DB, MovieNodeType);
        i++;
    }
    printf("Here %i Movies\n", i);

    printf("Clearing the previous values..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Successfully\n");

    closeDB(DB);
    if (getOccupiedMemory() == 0)
        printf("The memory has been properly freed!\n");
    else
        printf("Remaining: %i bytes!\n", getOccupiedMemory());

}

void test_select_and_update()
{
    printf("%s----------------------%s\n", WHITE, RESET);
    printf("\n\nTEST_SELECT_AND_UPDATE%s|%s\n", WHITE, RESET);
    printf("%s----------------------%s\n", WHITE, RESET);
    double start;

    char* Titles[nMovies] = { "The Shining", "7 moments of string", "The Green Mile", "The Shawshank Redemption", "A Nightmare on Elm Street", "Forrest Gump", "The House That Jack Built", "Interstellar", "Silent Hill", "The Illusionist" };
    char* Families[10] = { "Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio" };
    int i;

    initGraphsData("data.cfg");

    memDBScheme* Scheme = createScheme();

    nodeStruct* MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB* DB = createNew(Scheme, "graphs.mydb");

    memCondition* cond = createIntOrBoolAttrCondition(opLess, "Year", 2004);
    for (i = 0; i < nMovies * 5000; i++)
    {
        char* Title = gen_rand_str();
        if (i % 5000 == 0)
        {
            start = seconds();

            memNodeSetItem* ns = queryCypherStyle(DB, 1, MovieNodeType, cond);

            freeNodeSet(DB, ns);

            printf("Selecting %d rows took %lf seconds\n", i, (seconds() - start));

            start = seconds();

            setCypherStyle(DB, "Year", 1975, 1, MovieNodeType, cond);

            printf("Update operation on %d rows took %lf seconds\n", i, (seconds() - start));
        }
        createNode(DB, MovieNodeType);
        setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Title));
        free(Title);
        setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
        postNode(DB, MovieNodeType);
    }

    printf("Clearing the previous values..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Successfully\n");

    closeDB(DB);
    freeCondition(cond);

    if (getOccupiedMemory() == 0)
        printf("The memory has been properly freed!\n");
    else
        printf("Remaining: %i bytes!\n", getOccupiedMemory());

}

void test_delete()
{
    printf("%s-----------%s\n", WHITE, RESET);
    printf("\n\nTEST_DELETE%s|%s\n", WHITE, RESET);
    printf("%s-----------%s\n", WHITE, RESET);
    double start;

    char* Titles[nMovies] = { "The Shining", "7 moments of string", "The Green Mile", "The Shawshank Redemption", "A Nightmare on Elm Street", "Forrest Gump", "The House That Jack Built", "Interstellar", "Silent Hill", "The Illusionist" };
    char* Families[10] = { "Ivanov", "Sokolova", "Churikova", "Pinchuk", "Shapovalova", "Mikhailov", "Don", "Yeager", "Moroz", "Caprio" };
    int i;

    initGraphsData("data.cfg");

    memDBScheme* Scheme = createScheme();

    nodeStruct* MovieNodeType = addNodeTypeToScheme(Scheme, "Movie");
    addAttrToNodeScheme(MovieNodeType, "Title", aString);
    addAttrToNodeScheme(MovieNodeType, "Year", aInt32);

    memDB* DB = createNew(Scheme, "graphs.mydb");

    memCondition* cond = createIntOrBoolAttrCondition(opLess, "Year", 2004);

    for (int j = 0; j < nMovies; j++) {
        for (i = 0; i < j * 5000; i++) {
            char* Title = gen_rand_str();
            createNode(DB, MovieNodeType);
            setNodeAttr(DB, MovieNodeType, "Title", createString(DB, Title));
            free(Title);
            setNodeAttr(DB, MovieNodeType, "Year", 2000 + i);
            postNode(DB, MovieNodeType);
        }
        start = seconds();

        deleteCypherStyle(DB, 1, MovieNodeType, cond);

        printf("Deleting %i rows took %lf seconds\n", j * 5000, (seconds() - start));
    }

    printf("Clearing the previous values..\n");
    deleteCypherStyle(DB, 1, MovieNodeType, NULL);
    printf("Successfully\n");

    closeDB(DB);
    freeCondition(cond);

    if (getOccupiedMemory() == 0)
        printf("The memory has been properly freed!\n");
    else
        printf("Remaining: %i bytes!\n", getOccupiedMemory());

}

int main()
{
    test_insert();
    test_select_and_update();
    test_delete();
    return 0;
}